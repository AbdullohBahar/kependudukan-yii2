<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => $_ENV['YII_DB_DSN'] ?? 'mysql:host=localhost;dbname=yii2basic',
    'username' => $_ENV['YII_DB_USERNAME'] ?? 'root',
    'password' => $_ENV['YII_DB_PASSWORD'] ?? '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
