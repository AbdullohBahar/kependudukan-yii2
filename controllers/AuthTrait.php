<?php

namespace app\controllers;

use yii\filters\AccessControl;

trait AuthTrait
{
    use \jeemce\controllers\AppRoleTrait;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
