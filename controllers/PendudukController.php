<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Penduduk;
use app\models\Provinsi;
use app\models\Kabupaten;
use yii\base\DynamicModel;
use yii\widgets\ActiveForm;
use jeemce\helpers\DBHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use jeemce\controllers\AppCrudTrait;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Cell\DataType;


class PendudukController extends Controller
{
    use AuthTrait;
    use AppCrudTrait;

    private $modelClass = Penduduk::class;

    public function actionIndex()
    {
        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter' => [
                'regencies.province_id' => null,
                'regency_id' => null,
            ],
        ], $this->request->queryParams));

        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->joinWith(['provinsi', 'kabupaten'])
            ->andFilterWhere($searchModel->filter)
            ->andFilterWhere(
                [
                    'or',
                    ['like', 'resident_name', $searchModel->searchValue, false],
                    ['like', 'nik', $searchModel->searchValue, false],
                    ['like', 'birth_date', $searchModel->searchValue, false],
                    ['like', 'gender', $searchModel->searchValue, false],
                    ['like', 'address', $searchModel->searchValue, false],
                    ['like', 'provinces.province_name', $searchModel->searchValue, false],
                    ['like', 'regencies.regency_name', $searchModel->searchValue, false],
                ]
            );

        $provinsi = Provinsi::options('id', 'province_name');

        $provinceId = $searchModel->filter['regencies.province_id'];
        $kabupaten = Kabupaten::options('id', 'regency_name', ['regencies.province_id' => $provinceId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('index', get_defined_vars());
    }

    public function actionForm($id = null)
    {
        $class = $this->modelClass;

        if ($id) {
            $model = $this->findModel([
                'id' => $id
            ]);
        } else {
            $model = new $class;
        }

        if (($result = $this->save($model, ['index']))) {
            return $result;
        }

        $provinsi = Provinsi::options('id', 'province_name');
        $model->province_id = $this->request->get('province_id') ?? $model->province_id;
        $kabupaten = Kabupaten::options('id', 'regency_name', ['province_id' => $model->province_id]);

        return $this->render('form', get_defined_vars());
    }

    public function actionExport()
    {
        $templatePath = Yii::getAlias('@app/views/penduduk/template-export-penduduk.xlsx');

        $spreadsheet = IOFactory::load($templatePath);

        $sheet = $spreadsheet->getActiveSheet();

        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter' => [
                'regencies.province_id' => null,
                'regency_id' => null,
            ],
        ], Yii::$app->request->queryParams));

        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->joinWith(['provinsi', 'kabupaten'])
            ->andFilterWhere($searchModel->filter)
            ->andFilterWhere(
                [
                    'or',
                    ['like', 'resident_name', $searchModel->searchValue, false],
                    ['like', 'nik', $searchModel->searchValue, false],
                    ['like', 'birth_date', $searchModel->searchValue, false],
                    ['like', 'gender', $searchModel->searchValue, false],
                    ['like', 'address', $searchModel->searchValue, false],
                    ['like', 'provinces.province_name', $searchModel->searchValue, false],
                    ['like', 'regencies.regency_name', $searchModel->searchValue, false],
                ]
            )->all();

        $spreadsheet->getProperties()
            ->setTitle('Laporan Penduduk');

        $borderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
        ];

        $row = 4;
        foreach ($query as $data) {
            $sheet->setCellValueExplicit('A' . $row, $data->nik, DataType::TYPE_STRING);
            $sheet->setCellValue('B' . $row, $data->resident_name);
            $sheet->setCellValue('C' . $row, $data->age);
            $sheet->setCellValue('D' . $row, $data->fullAddress);

            // set border
            $sheet->getStyle('A' . $row . ':D' . $row)->applyFromArray($borderStyle);

            $row++;
        }

        // Auto size columns
        foreach (range('A', 'D') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        // Set headers for file download
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Penduduk.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    protected function findModel($params)
    {
        if (($model = $this->modelClass::findOne($params))) return $model;
        throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
    }
}
