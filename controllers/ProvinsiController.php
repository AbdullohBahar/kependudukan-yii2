<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Provinsi;
use yii\base\DynamicModel;
use yii\widgets\ActiveForm;
use jeemce\helpers\DBHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use jeemce\controllers\AppCrudTrait;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;


class ProvinsiController extends Controller
{
    use AuthTrait;
    use AppCrudTrait;

    private $modelClass = Provinsi::class;

    public function actionIndex()
    {
        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter',
        ], $this->request->queryParams));

        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->andFilterWhere(
                ['like', 'province_name', $searchModel->searchValue, false]
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('index', get_defined_vars());
    }

    public function actionForm($id = null)
    {
        $class = $this->modelClass;

        if ($id) {
            $model = $this->findModel([
                'id' => $id
            ]);
        } else {
            $model = new $class;
        }

        if (($result = $this->save($model, ['index']))) {
            return $result;
        }

        return $this->render('form', get_defined_vars());
    }

    public function actionLaporan()
    {
        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter',
        ], $this->request->queryParams));


        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->andFilterWhere(
                ['like', 'province_name', $searchModel->searchValue, false]
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('laporan', get_defined_vars());
    }

    public function actionExport()
    {
        $templatePath = Yii::getAlias('@app/views/provinsi/template-export-provinsi.xlsx');

        $spreadsheet = IOFactory::load($templatePath);

        $sheet = $spreadsheet->getActiveSheet();

        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter',
        ], $this->request->queryParams));


        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->andFilterWhere(
                ['like', 'province_name', $searchModel->searchValue, false]
            )->all();

        $spreadsheet->getProperties()
            ->setTitle('Laporan Provinsi');

        $borderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
        ];

        $row = 4;
        foreach ($query as $data) {
            $sheet->setCellValue('A' . $row, $data->province_name);
            $sheet->setCellValue('B' . $row, $data->hitungPenduduk);

            // set border
            $sheet->getStyle('A' . $row . ':B' . $row)->applyFromArray($borderStyle);

            $row++;
        }

        // Auto size columns
        foreach (range('A', 'B') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Provinsi.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    protected function findModel($params)
    {
        if (($model = $this->modelClass::findOne($params))) return $model;
        throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
    }
}
