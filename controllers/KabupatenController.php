<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Provinsi;
use app\models\Kabupaten;
use yii\base\DynamicModel;
use yii\widgets\ActiveForm;
use jeemce\helpers\DBHelper;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use jeemce\controllers\AppCrudTrait;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

class KabupatenController extends Controller
{
    use AuthTrait;
    use AppCrudTrait;

    private $modelClass = Kabupaten::class;

    public function actionIndex()
    {
        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter' => [
                'province_id' => null
            ],
        ], $this->request->queryParams));


        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->alias('kab')
            ->joinWith('provinsi')
            ->andFilterWhere($searchModel->filter)
            ->andFilterWhere(
                [
                    'or',
                    ['like', 'regency_name', $searchModel->searchValue, false],
                    ['like', 'provinces.province_name', $searchModel->searchValue, false],
                ]
            );

        $provinsi = Provinsi::options('id', 'province_name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('index', get_defined_vars());
    }

    public function actionForm($id = null)
    {
        $class = $this->modelClass;

        if ($id) {
            $model = $this->findModel([
                'id' => $id
            ]);
        } else {
            $model = new $class;
        }

        if (($result = $this->save($model, ['index']))) {
            return $result;
        }

        $provinsi = Provinsi::options('id', 'province_name');

        return $this->render('form', get_defined_vars());
    }


    public function actionLaporan()
    {
        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter' => [
                'regencies.province_id' => null
            ],
        ], $this->request->queryParams));


        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->joinWith('provinsi')
            ->andFilterWhere($searchModel->filter)
            ->andFilterWhere(
                [
                    'or',
                    ['like', 'regency_name', $searchModel->searchValue, false],
                    ['like', 'provinces.province_name', $searchModel->searchValue, false],
                ]
            );

        $provinsi = Provinsi::options('id', 'province_name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('laporan', get_defined_vars());
    }

    public function actionExport()
    {
        $templatePath = Yii::getAlias('@app/views/kabupaten/template-export-kabupaten.xlsx');

        $spreadsheet = IOFactory::load($templatePath);

        $sheet = $spreadsheet->getActiveSheet();

        $searchModel = new DynamicModel(array_merge([
            'searchValue',
            'search',
            'filter' => [
                'province_id' => null
            ],
        ], $this->request->queryParams));


        if (!empty($searchModel->search)) {
            $searchModel->searchValue = DBHelper::queryLikeValue($searchModel->search);
        }

        $query = $this->modelClass::find()
            ->joinWith('provinsi')
            ->andFilterWhere($searchModel->filter)
            ->andFilterWhere(
                [
                    'or',
                    ['like', 'regency_name', $searchModel->searchValue, false],
                    ['like', 'provinces.province_name', $searchModel->searchValue, false],
                ]
            )->all();

        $spreadsheet->getProperties()
            ->setTitle('Laporan Kabupaten');

        $borderStyle = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
        ];

        $row = 4;
        foreach ($query as $data) {
            $sheet->setCellValue('A' . $row, $data->regency_name);
            $sheet->setCellValue('B' . $row, $data->provinsi?->province_name);
            $sheet->setCellValue('C' . $row, $data->hitungPenduduk);

            // set border
            $sheet->getStyle('A' . $row . ':C' . $row)->applyFromArray($borderStyle);

            $row++;
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Kabupaten.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit;
    }

    protected function findModel($params)
    {
        if (($model = $this->modelClass::findOne($params))) return $model;
        throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
    }
}
