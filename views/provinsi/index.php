<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use jeemce\grid\ActionColumn;
use jeemce\grid\SerialColumn;
use jeemce\helpers\TwbsHelper;
use jeemce\helpers\WidgetHelper;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
$this->title = 'Data Provinsi';

?>

<?php Pjax::begin(['options' => ['class' => 'card']]) ?>

<?php $form = ActiveForm::begin([
    'action' => 'index',
    'method' => 'get',
    'options' => [
        'data-pjax' => 1,
        'class' => 'card-header'
    ]
]) ?>

<div class="row">
    <div class="col-6">
        <?= Html::a(
            'Tambah',
            Url::current(['form']),
            ['class' => 'btn btn-primary mr-2'],
        ) ?>
    </div>
    <div class="col-6">
        <?=
        Html::input('text', 'search', $searchModel->search, [
            'class' => 'form-control',
            'placeholder' => 'cari'
        ])
        ?>
    </div>
</div>

<?php ActiveForm::end() ?>

<div class="card-body">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => ['class' => 'pagination d-none'],
        ],
        'columns' => [
            ['class' => SerialColumn::class],
            'province_name',
            [
                'class' => ActionColumn::class,
            ],
        ]
    ]);
    ?>
</div>

<div class="card-footer d-flex align-items-center">
    <div class="col-md">
        <?= WidgetHelper::providerSummary($dataProvider) ?>
    </div>

    <div class="col-md-auto">
        <?= TwbsHelper::linkPagerClass()::widget(['pagination' => $dataProvider->pagination]) ?>
    </div>
</div>

<?php Pjax::end(); ?>