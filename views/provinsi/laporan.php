<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use jeemce\grid\SerialColumn;
use jeemce\helpers\TwbsHelper;
use jeemce\helpers\WidgetHelper;

/** @var yii\web\View $this */
$this->title = 'Data Provinsi';

?>

<?php Pjax::begin(['options' => ['class' => 'card']]) ?>

<?php $form = ActiveForm::begin([
    'action' => 'laporan',
    'method' => 'get',
    'options' => [
        'data-pjax' => true,
        'class' => 'card-header'
    ]
]) ?>

<div class="row">
    <div class="col-6">
        <?= Html::a(
            'Export',
            Url::current(['export']),
            [
                'class' => 'btn btn-primary mr-2',
                'data-pjax' => 0,
                'target' => '_blank'
            ],
        ) ?>
    </div>
    <div class="col-6">
        <?=
        Html::input('text', 'search', $searchModel->search, [
            'class' => 'form-control',
            'placeholder' => 'cari',
        ])
        ?>
    </div>
</div>


<?php ActiveForm::end() ?>

<div class="card-body">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => ['class' => 'pagination d-none'],
        ],
        'columns' => [
            ['class' => SerialColumn::class],
            'province_name',
            [
                'attribute' => 'hitungPenduduk',
                'label' => 'Banyak Penduduk',
            ],
        ]
    ]);
    ?>
</div>

<div class="card-footer d-flex align-items-center">
    <div class="col-md">
        <?= WidgetHelper::providerSummary($dataProvider) ?>
    </div>

    <div class="col-md-auto">
        <?= TwbsHelper::linkPagerClass()::widget(['pagination' => $dataProvider->pagination]) ?>
    </div>
</div>

<?php Pjax::end(); ?>