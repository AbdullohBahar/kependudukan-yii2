<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
$this->title = 'Tambah Kabupaten';
?>
<div class="site-index">
    <?php $form = ActiveForm::begin([
        'id' => 'form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'regency_name')->input('text') ?>

    <?= $form->field($model, 'province_id')->dropDownList($provinsi, [
        'prompt' => 'Pilih Provinsi'
    ]) ?>

    <?= Html::submitButton('Submit', [
        'class' => 'btn btn-success'
    ]) ?>

    <?php ActiveForm::end(); ?>
</div>