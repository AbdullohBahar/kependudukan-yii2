<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Penduduk;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
$this->title = 'Tambah Penduduk';
$genderOptions = Penduduk::genderOptions();

$this->registerJs("
    $('body').on('change','#province', function() {
		var id = $(this).val()
		$.pjax.reload('#get-pjax', {
			url: $('#get-pjax').data('url').replace('__id__', id)
		})
    });
");

?>
<div class="site-index">
    <?php $form = ActiveForm::begin([
        'id' => 'form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'resident_name')->input('text') ?>
    <?= $form->field($model, 'nik')->input('text') ?>
    <?= $form->field($model, 'birth_date')->input('date') ?>
    <?= $form->field($model, 'address')->textarea() ?>

    <?= $form->field($model, 'gender')->dropDownList($genderOptions, [
        'prompt' => 'Pilih Gender'
    ]) ?>

    <?php Pjax::begin([
        'id' => 'get-pjax',
        'clientOptions' => [
            'type' => 'GET'
        ],
        'options' => [
            'data-url' => Url::current(
                [
                    'province_id' => '__id__'
                ]
            )
        ]
    ]); ?>

    <?= $form->field($model, 'province_id')->dropDownList($provinsi, [
        'prompt' => 'Pilih Provinsi',
        'id' => 'province'
    ])->label('Provinsi') ?>

    <?= $form->field($model, 'regency_id')->dropDownList($kabupaten, [
        'prompt' => 'Pilih Kabupaten'
    ])->label('Kabupaten') ?>

    <?php Pjax::end() ?>

    <?= Html::submitButton('Submit', [
        'class' => 'btn btn-success'
    ]) ?>

    <?php ActiveForm::end(); ?>
</div>