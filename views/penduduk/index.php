<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use jeemce\grid\ActionColumn;
use jeemce\grid\SerialColumn;
use jeemce\helpers\TwbsHelper;
use jeemce\helpers\WidgetHelper;

/** @var yii\web\View $this */
$this->title = 'Data Penduduk';

$this->registerJs("
    $('body').on('change','#province', function() {
		$('#regency').val('')
    });
");
?>
<?php Pjax::begin(['options' => ['class' => 'card']]) ?>

<?php $form = ActiveForm::begin([
    'action' => 'index',
    'method' => 'get',
    'options' => [
        'data-pjax' => 1,
        'class' => 'card-header'
    ]
]) ?>

<div class="row mt-2">
    <div class="col-12">
        <?= Html::a(
            'Tambah',
            Url::current(['form']),
            ['class' => 'btn btn-primary mr-2'],
        ) ?>

        <?= Html::a(
            'Export',
            Url::current(['export']),
            [
                'class' => 'btn btn-success mr-2 ',
                'data-pjax' => 0,
                'target' => '_blank'
            ],
        ) ?>
    </div>
</div>

<div class="row mt-2">
    <div class="col-4">
        <?=
        Html::input('text', 'search', $searchModel->search, [
            'class' => 'form-control',
            'placeholder' => 'cari'
        ])
        ?>
    </div>

    <div class="col-4">
        <?= Html::dropDownList('filter[regencies.province_id]', $searchModel->filter['regencies.province_id'], $provinsi, [
            'prompt' => 'Pilih Provinsi',
            'class' => 'form-select',
            'id' => 'province',
            'onchange' => "$(this.form).trigger('submit')",
        ]) ?>
    </div>
    <div class="col-4">
        <?= Html::dropDownList('filter[regency_id]', $searchModel->filter['regency_id'], $kabupaten, [
            'prompt' => 'Pilih Kabupaten',
            'id' => 'regency',
            'class' => 'form-select',
            'onchange' => "$(this.form).trigger('submit')",
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>

<div class="card-body">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => ['class' => 'pagination d-none'],
        ],
        'columns' => [
            ['class' => SerialColumn::class],
            'nik',
            'resident_name',
            'age',
            'fullAddress',
            [
                'class' => ActionColumn::class,
            ],
        ]
    ]);
    ?>
</div>

<div class="card-footer d-flex align-items-center">
    <div class="col-md">
        <?= WidgetHelper::providerSummary($dataProvider) ?>
    </div>

    <div class="col-md-auto">
        <?= TwbsHelper::linkPagerClass()::widget(['pagination' => $dataProvider->pagination]) ?>
    </div>
</div>

<?php Pjax::end(); ?>