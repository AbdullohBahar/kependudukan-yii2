<?php

namespace app\models;

use Yii;

class Provinsi extends \jeemce\models\Model
{
    public static function tableName()
    {
        return 'provinces';
    }

    public function attributeLabels()
    {
        return [
            'province_name' => 'Nama Provinsi',
        ];
    }

    public function rules()
    {
        return [
            [['province_name'], 'required'],
            [['province_name'], 'unique'],
        ];
    }

    public function getKabupaten()
    {
        return $this->hasMany(Kabupaten::class, ['province_id' => 'id']);
    }

    public function getPenduduk()
    {
        return $this->hasMany(Penduduk::class, ['province_id' => 'id']);
    }

    public function getHitungPenduduk()
    {
        return $this->hasMany(Penduduk::class, ['province_id' => 'id'])->count();
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->getKabupaten()->exists() || $this->getPenduduk()->exists()) {
                Yii::$app->session->setFlash('saveFail', 'Data gagal dihapus, data masih digunakan.');
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}
