<?php

namespace app\models;

use Yii;
use DateTime;
use jeemce\helpers\ArrayHelper;

class Penduduk extends \jeemce\models\Model
{
    public static function tableName()
    {
        return 'residents';
    }

    public function attributeLabels()
    {
        return [
            'provinsi.province_name' => 'Nama Provinsi',
            'kabupaten.regency_name' => 'Nama Kabupaten',
            'resident_name' => 'Nama Penduduk',
            'nik' => 'NIK',
            'birth_date' => 'Tanggal Lahir',
            'gender' => 'Jenis Kelamin',
            'address' => 'Alamat',
            'regency_id' => 'Kabupaten',
            'province_id' => 'Provinsi',
            'fullAddress' => 'Alamat',
            'age' => 'Umur',
        ];
    }

    public function rules()
    {
        return [
            [['regency_id', 'province_id', 'resident_name', 'nik', 'birth_date', 'gender', 'address'], 'required'],
            [['nik'], 'unique'],
            [['nik'], 'string', 'max' => 16]
        ];
    }

    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::class, ['id' => 'province_id']);
    }

    public function getKabupaten()
    {
        return $this->hasOne(Kabupaten::class, ['id' => 'regency_id']);
    }

    public function getFullAddress()
    {
        $address = $this->address ?? '-';
        $kabupaten = $this->kabupaten->regency_name ?? '-';
        $provinsi = $this->provinsi->province_name ?? '-';

        return "$address, $kabupaten, $provinsi";
    }

    public function getAge()
    {
        $birthDate = new DateTime($this->birth_date);
        $today = new DateTime();
        $age = $today->diff($birthDate)->y;
        return $age;
    }

    public static function genderOptions()
    {
        return ArrayHelper::assoc([
            'L' => 'Laki-Laki',
            'P' => 'Perempuan',
        ], 'ucfirst');
    }
}
