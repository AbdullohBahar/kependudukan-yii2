<?php

namespace app\models;

use Yii;

class Kabupaten extends \jeemce\models\Model
{
    public static function tableName()
    {
        return 'regencies';
    }

    public function attributeLabels()
    {
        return [
            'provinsi.province_name' => 'Nama Provinsi',
            'regency_name' => 'Nama Kabupaten',
            'province_id' => 'Nama Provinsi',
        ];
    }

    public function rules()
    {
        return [
            [['regency_name', 'province_id'], 'required'],
        ];
    }

    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::class, ['id' => 'province_id']);
    }

    public function getHitungPenduduk()
    {
        return $this->hasMany(Penduduk::class, ['regency_id' => 'id'])->count();
    }

    public function getPenduduk()
    {
        return $this->hasMany(Penduduk::class, ['regency_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->getPenduduk()->exists()) {
                Yii::$app->session->setFlash('saveFail', 'Data gagal dihapus, data masih digunakan.');
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}
